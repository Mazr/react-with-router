import React from 'react';
import NotMatch from "./NotMatch";

const User = (props) => {
  const reg = /^[1-9]+[0-9]*$/;
  const user = props.match.params.user;
  return (
    <div>
      { reg.test(user) ? (
        <div>
          User profile page.
        </div>
      ) : (<NotMatch/>)}
    </div>
      );
};
            export default User;