import data from '../../exercise-2/mockups/data'
import React from "react";
import {Redirect} from "react-router";

export const ProductDetail = props => {

  const urlId = props.match.params.id;
  const list = Object.values(data);
  const obj = list.find(obj => obj.id == urlId );
  const {name,id,price,quantity,desc} = obj;

  return (
    <div>
      {props.history.action === 'POP' ? <Redirect to={'/error'}/> : (
        <ul>
          <li>Product Detail:</li>
          <li>Name:{name}</li>
          <li>Id:{id}</li>
          <li>Price:{price}</li>
          <li>Quantity:{quantity}</li>
          <li>Desc:{desc}</li>
          <li>URL:{props.location.pathname}</li>
        </ul>

      )
      }

    </div>
  )
};
