import React from 'react';
const Error = ({match}) => (
  <div>
    <h3>Can not go to product details page. Please go back to Products page.</h3>
  </div>
);
export default Error;