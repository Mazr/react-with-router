import React from 'react';
import {Link} from "react-router-dom";
const AboutUs = () => (
  <div>
    <ul>
      <li>Company:ThoughtWorks Local</li>
      <li>Location:Xi'an</li>
    </ul>
    <p>
      For more information,please view our
    <Link to={"/"}>website</Link>
    </p>
  </div>
);
export default AboutUs;