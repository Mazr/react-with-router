import React from 'react';

const MyProfile = ({ match }) => (
  <div>
    <ul>
      <li>Username:XXXX</li>
      <li>Gender:Female</li>
      <li>Work:IT Industry</li>
      <li>Website:'{match.path}'</li>
    </ul>
  </div>
);
export default MyProfile;