import React from 'react';
import {NavLink} from "react-router-dom";
import data from "../../exercise-2/mockups/data";

const Products = ({ match }) => (
  <div>
    <ul>
      <li>All Products</li>
      <li><NavLink to={`${match.path}/${data.bicycle.id}`}>{data.bicycle.name}</NavLink></li>
      <li><NavLink to={`${match.path}/${data.TV.id}`}>{data.TV.name}</NavLink></li>
      <li><NavLink to={`${match.path}/${data.pencil.id}`}>{data.pencil.name}</NavLink></li>
    </ul>
  </div>
);

export default Products;