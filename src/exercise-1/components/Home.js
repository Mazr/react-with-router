import React from 'react';
const Home = ({match}) => (
  <div>
    <h3>This is a beautiful Home Page.</h3>
    <h3>And the url is'{match.path}'.</h3>
  </div>
);
export default Home;