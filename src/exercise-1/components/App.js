import React from 'react';
import '../styles/App.css';
import {
  BrowserRouter as Router,
  Route,
  NavLink, Redirect,
} from 'react-router-dom'
import Home from "./Home";
import MyProfile from "./MyProfile";
import AboutUs from "./AboutUs";
import Products from "./Products";
import {ProductDetail} from "./ProductDetail";
import Error from "./Error";
import {Switch} from "react-router";

const App = () => (
  <Router>
    <div>
      <header className={'header'}>
        <ul>
          <li><NavLink exact to="/" activeStyle={{textDecoration: 'underline'}}>Home</NavLink></li>
          <li><NavLink to="/products" activeStyle={{textDecoration: 'underline'}}>Products</NavLink></li>
          <li><NavLink to="/my-profile" activeStyle={{textDecoration: 'underline'}}>MyProfile</NavLink></li>
          <li><NavLink to="/about-us" activeStyle={{textDecoration: 'underline'}}>About Us</NavLink></li>
        </ul>
      </header>

      <div className={'body'}>
      <Switch>
        <Route exact path={'/'} component={Home}/>
        <Route path='/my-profile' component={MyProfile}/>
        <Route exact path='/products' component={Products}/>
        <Redirect from={'/goods'} to={'products'}/>
        <Route path={'/products/:id'} component={ProductDetail}/>
        <Route path="/about-us" component={AboutUs}/>
        <Route path={'/error'} component={Error}/>
      </Switch>
      </div>
    </div>
  </Router>
)

export default App;
